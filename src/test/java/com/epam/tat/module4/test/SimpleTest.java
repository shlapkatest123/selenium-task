package com.epam.tat.module4.test;

import com.epam.tat.module4.driver.DriverSingleton;
import com.epam.tat.module4.page.*;
import com.epam.tat.module4.utils.TabManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
//blablabla

public class SimpleTest {
    TabManager tabManager;
    WebDriver driver;

    @BeforeEach
    void setup() {
        driver = DriverSingleton.getDriver();
        tabManager = new TabManager(driver);
    }
    @Test
    void openBrowser() {
        GoogleCalculatorIframePage.Estimate estimate = new CloudStartPage(driver).openPage().DoSearch("Google Cloud Platform Pricing Calculator").openGoogleCalculatorPage().switchToCalculatorIframe().setNumberOfInstances(4).selectOperatingSystem("Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)").selectProvisioningModel("Regular").selectMachineFamily("General purpose").selectSeries("N1").selectMachineType("n1-standard-8 (vCPUs: 8, RAM: 30GB)").addGPUs("NVIDIA Tesla V100", "1").selectLocalSSD("2x375 GB").selectDataCenter("Frankfurt (europe-west3)").selectCommitedUsage("1 Year").addToEstimate();
        double totalCost = estimate.getTotalCost(); //getting cost from Google Calculator
        tabManager.openNewTab(""); // switch to new empty tab
        YOPMailCheckPage checkPage = new YOPMailPage(driver).openPage().getEmailPage().checkMail(); //open YOPMAIL in new tab and gen an email
        String email = checkPage.getMailAddress();  //getting an email from YOPMAIL
        tabManager.switchToTab("Calculator"); //switch to Google Calculator
        new GoogleCloudPricingCalcPage(driver).switchToCalculatorIframe(); //focus on the page's iframe
        estimate.sendOnEmail(email);//send data to email
        tabManager.switchToTabByURL("yopmail.com"); //switching to YOPMAIL
        double totalCost2 = checkPage.getPriceFromMessage(); //getting cost from the YOPMAIL
        Assertions.assertEquals(totalCost, totalCost2); //final Assert
    }

    @AfterEach
    void cleanUp() {
        driver.quit();
    }
}
