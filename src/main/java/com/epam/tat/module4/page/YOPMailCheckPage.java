package com.epam.tat.module4.page;

import com.epam.tat.module4.utils.ParseUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.epam.tat.module4.waits.CustomConditions;

import java.util.Locale;

public class YOPMailCheckPage extends AbstractPage {
    @FindBy(id = "refresh")
    private WebElement refreshButton;
    @FindBy(xpath = "//*[@class='bname']")
    private WebElement mailAddress;
    @FindBy(id = "ifmail")
    private WebElement MessageIframe;

    public YOPMailCheckPage(WebDriver driver) {
        super(driver);
    }

    public String getMailAddress() {
        new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.visibilityOf(mailAddress));
        return mailAddress.getText();
    }

    public double getPriceFromMessage() {
        String textTobeChanged = driver.findElement(By.id("nbmail")).getText();
        new WebDriverWait(driver, TIMEOUT).until(CustomConditions.clickOnElementTillTextChanges(refreshButton, "nbmail", textTobeChanged));
        driver.switchTo().frame(MessageIframe);
        WebElement cost = new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[contains(text(), 'USD')]")));
        String text = cost.getText().trim();
        String numericString = text.replaceAll("[^\\d.]", "");
        return ParseUtil.parseStringToDouble(numericString, Locale.US);
    }
}
