package com.epam.tat.module4.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TabManager {
    private WebDriver driver;

    public TabManager(WebDriver driver) {
        this.driver = driver;
    }

    public TabManager switchToTab(String tabTitle) {
        Set<String> windowHandles = driver.getWindowHandles();
        for (String handle : windowHandles) {
            driver.switchTo().window(handle);
            if (driver.getTitle().contains(tabTitle)) {
                return this;
            }
        }
        throw new RuntimeException("Tab with title '" + tabTitle + "' not found");
    }

    public TabManager switchToTabByURL(String tabTitle) {
        Set<String> windowHandles = driver.getWindowHandles();
        for (String handle : windowHandles) {
            driver.switchTo().window(handle);
            if (driver.getCurrentUrl().contains(tabTitle)) {
                return this;
            }
        }
        throw new RuntimeException("Tab with url '" + tabTitle + "' not found");
    }
    public TabManager switchToLastTab() {
        List<String> windowHandles = new ArrayList<>(driver.getWindowHandles());
        if (!windowHandles.isEmpty()) {
            driver.switchTo().window(windowHandles.get(windowHandles.size() - 1));
        } else {
            throw new RuntimeException("No tabs found");
        }
        return this;
    }

    public TabManager closeCurrentTab() {
        driver.close();
        return this;
    }

    public TabManager openNewTab(String url) {
        driver.switchTo().newWindow(WindowType.TAB);
        return this;
    }
}
