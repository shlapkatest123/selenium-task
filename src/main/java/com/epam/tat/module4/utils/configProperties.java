package com.epam.tat.module4.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class configProperties {
    public static Properties getProperty() {
        if (property == null) initializaPropertyFile();
        return property;
    }

    private static Properties property;
    private static String configPath = "src/main/resources/configuration.properties";

    public static void initializaPropertyFile() {
        property = new Properties();
        try {
            InputStream instream = new FileInputStream(configPath);
            try {
                property.load(instream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
