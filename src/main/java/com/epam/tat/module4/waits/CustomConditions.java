package com.epam.tat.module4.waits;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;

public class CustomConditions {
    public static ExpectedCondition<Boolean> JSpageReady() {
        return (driver) -> {
            return ((JavascriptExecutor) driver).executeScript("return document.readyState")
                    .equals("complete");
        };
    }
    public static ExpectedCondition<Boolean> jQueryAJAXsCompleted() {
        return (driver) -> {
            return (Boolean) ((JavascriptExecutor) driver).executeScript("return (window.jQuery != null) && (jQuery.active === 0); ");
        };
    }

    public static ExpectedCondition<Boolean> clickOnElementTillTextChanges(WebElement elementToBeClicked, String iDOfElementToBeChanged, String textBefore) {
        return (driver) -> {
            elementToBeClicked.click();
            String text = driver.findElement(By.id(iDOfElementToBeChanged)).getText();
            return !textBefore.equals(text);
        };
    }

}
